import { Directive, OnInit, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appClickDisappear]',
  host: {
    '(click)': 'vanish()'
  }
})
export class ClickDisappearDirective implements OnInit {

  constructor(private element: ElementRef, private renderer: Renderer2) { 

  }

  ngOnInit(){
    this.renderer.setStyle(this.element.nativeElement, 'transition', 'opacity 2s linear');
  }

  vanish(){
    this.renderer.setStyle(this.element.nativeElement, 'opacity', '0');
  }

}
